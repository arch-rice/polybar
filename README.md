To work it correctly u may need to make some changes. Refer to polybar wiki.

The fonts I use can be found in config file. Just open it & search for "font".

The polybar is configured to use with pywal. (It automatically changes color scheme based on wallpaper)

![](https://gitlab.com/arch-rice/polybar/raw/master/polybar2.png?raw=true)
